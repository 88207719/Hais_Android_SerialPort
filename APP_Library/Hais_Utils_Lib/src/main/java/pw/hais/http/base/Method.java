package pw.hais.http.base;

/**
 * Created by Hais1992 on 2015/8/25.
 */
public enum Method {
    POST("POST"),
    GET("GET"),
    PUT("PUT"),
    DELETE("DELETE"),
    PATCH("PATCH");


    public String type;

    Method(String type) {
        this.type = type;
    }
}
