package pw.hais.http;

import android.graphics.Bitmap;
import android.net.Uri;
import android.widget.ImageView;

import org.json.JSONObject;

import java.io.File;
import java.util.Map;

import pw.hais.http.base.BaseHttp;
import pw.hais.http.base.Method;
import pw.hais.http.base.OnHttpListener;
import pw.hais.http.image.CacheManager;

/**
 * 基于 OkHttp 的网络请求封装
 * Created by Hais1992 on 2015/8/25.
 */
public class Http {

    public static void get(String url, Map<String, String> params, OnHttpListener<?> listener) {
        get(url, null, params, listener);
    }

    public static void get(String url, Map<String, String> header, Map<String, String> params, OnHttpListener<?> listener) {
        BaseHttp.addHttpRequest(Method.GET, url, header, params, null, listener);
    }

    public static void post(String url, Map<String, String> params, OnHttpListener<?> listener) {
        post(url, null, params, listener);
    }

    public static void post(String url, Map<String, String> header, Map<String, String> params, OnHttpListener<?> listener) {
        BaseHttp.addHttpRequest(Method.POST, url, header, params, null, listener);
    }

    public static void post(String url, Map<String, String> header, Map<String, String> params, byte[] body, OnHttpListener<?> listener) {
        BaseHttp.addHttpRequest(Method.POST, url, header, params, body, listener);
    }

    public static void post(String url, Map<String, String> header, Map<String, String> params, Map<String, String> body, OnHttpListener<?> listener) {
        BaseHttp.addHttpRequest(Method.POST, url, header, params, body, listener);
    }

    public static void post(String url, Map<String, String> header, Map<String, String> params, String body, OnHttpListener<?> listener) {
        BaseHttp.addHttpRequest(Method.POST, url, header, params, body, listener);
    }

    public static void post(String url, Map<String, String> header, Map<String, String> params, JSONObject body, OnHttpListener<?> listener) {
        BaseHttp.addHttpRequest(Method.POST, url, header, params, body, listener);
    }

    public static void post(String url, Map<String, String> header, Map<String, String> params, File body, OnHttpListener<?> listener) {
        BaseHttp.addHttpRequest(Method.POST, url, header, params, body, listener);
    }


    public static void put(String url, Map<String, String> header, Map<String, String> params, JSONObject body, OnHttpListener<?> listener) {
        BaseHttp.addHttpRequest(Method.PUT, url, header, params, body, listener);
    }

    public static void put(String url, Map<String, String> header, Map<String, String> params, Map<String, String> body, OnHttpListener<?> listener) {
        BaseHttp.addHttpRequest(Method.PUT, url, header, params, body, listener);
    }

    public static void delete(String url, Map<String, String> header, Map<String, String> params, String body, OnHttpListener<?> listener) {
        BaseHttp.addHttpRequest(Method.DELETE, url, header, params, body, listener);
    }


    /*-----------------------------HTTP文件上传-------------------------------------*/
    public static void updateFile(String url, Map<String, String> header, Map<String, String> params, File[] files, String[] fileKeys, OnHttpListener<?> listener) {
        BaseHttp.addUpdateRequest(url, header, params, files, fileKeys, listener);
    }

    public static void updateFile(String url, Map<String, String> header, Map<String, String> params, File file, String fileKey, OnHttpListener<?> listener) {
        BaseHttp.addUpdateRequest(url, header, params, new File[]{file}, new String[]{fileKey}, listener);
    }

    /*-----------------------------图片下载显示-------------------------------------*/
    public static void displayImageFullSize(ImageView imageView, String url) {
        BaseHttp.addImageRequest(imageView, url, -1, -1, false, null);
    }

    public static void displayImageAutoSize(ImageView imageView, String url) {
        BaseHttp.addImageRequest(imageView, url, 0, 0, false, null);
    }

    public static void displayImage(ImageView imageView, String url, int maxWidth, int maxHeight) {
        BaseHttp.addImageRequest(imageView, url, maxWidth, maxHeight, false, null);
    }

    public static void displayImage(ImageView imageView, String url, int maxWidth, int maxHeight, boolean isCover, OnHttpListener<Bitmap> listener) {
        BaseHttp.addImageRequest(imageView, url, maxWidth, maxHeight, isCover, listener);
    }

    public static void cacheImage(String url) {
        BaseHttp.addImageRequest(null, url, -1, -1, false, null);
    }

    public static void cacheImage(String url, OnHttpListener<Bitmap> listener) {
        BaseHttp.addImageRequest(null, url, -1, -1, false, listener);
    }
    public static void cacheImage(String url, boolean isCover, OnHttpListener<Bitmap> listener) {
        BaseHttp.addImageRequest(null, url, -1, -1, isCover, listener);
    }

    public static Bitmap getCacheBitmapFullSize(String url) {
        Bitmap bitmap = CacheManager.getBitmapCache(url, -1, -1);
        return bitmap;
    }

    public static Bitmap getCacheBitmapAutoSize(String url) {
        Bitmap bitmap = CacheManager.getBitmapCache(url, 0, 0);
        return bitmap;
    }

    public static Bitmap getCacheBitmapAutoSize(Uri url) {
        Bitmap bitmap = CacheManager.getBitmapCache(url, 0, 0);
        return bitmap;
    }

    /*-----------------------------文件　　下载-------------------------------------*/
    public static void download(String url, String fileDir, OnHttpListener<String> listener) {
        BaseHttp.addDownloadRequest(url, fileDir, true, listener);
    }

    public static void download(String url, String fileDir, boolean isCover, OnHttpListener<String> listener) {
        BaseHttp.addDownloadRequest(url, fileDir, isCover, listener);
    }


    /*-----------------------------取消一个请求-------------------------------------*/
    public static void cancel(String url) {
        BaseHttp.cancel(url);
    }



}