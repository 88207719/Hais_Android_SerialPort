package pw.hais.view;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;

import pw.hais.helper.TintedBitmapDrawable;
import pw.hais.utils.ImageUtil;
import pw.hais.utils.R;

/**
 * 带选中，和点击效果 的  button
 * Created by hais1992 on 15-5-6.
 */
public class ButtonFocusView extends Button implements View.OnFocusChangeListener{
    private boolean select_state =false;    //选中状态
    private int focusColor,disableColor;

    public ButtonFocusView(Context context) {
        super(context);
        init(context,null);
    }

    public ButtonFocusView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context, attrs);
    }

    public ButtonFocusView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context, attrs);
    }

    private BitmapDrawable focusDrawable;
    private BitmapDrawable disableDrawable;
    private Bitmap defaultBitmap;
    private Drawable defaultDrawable;

    private void init(Context context, AttributeSet attrs){
        TypedArray array =  context.obtainStyledAttributes(attrs, R.styleable.FocusView);
        focusColor = array.getColor(R.styleable.FocusView_focus_color,0xff9e9e9e);
        disableColor = array.getColor(R.styleable.FocusView_disable_color,0xff9e9e9e);

        defaultDrawable = getBackground();  //获取当前背景图Drawable
        defaultBitmap = ImageUtil.drawableToBitmap(defaultDrawable);  //获取当前背景图Bitmap
        this.setDrawingCacheEnabled(true);
        this.setClickable(true);
        this.setOnFocusChangeListener(this);
    }

    @Override
    protected void dispatchDraw(Canvas canvas) {
        super.dispatchDraw(canvas);

        if(!isEnabled()){
            if(disableDrawable==null)disableDrawable = new TintedBitmapDrawable(getResources(),defaultBitmap,disableColor);
            setBackgroundDrawable(disableDrawable);
        }else{
            if (select_state) {
                if(focusDrawable==null)focusDrawable = new TintedBitmapDrawable(getResources(),defaultBitmap,focusColor);
                setBackgroundDrawable(focusDrawable);
            }else{
                setBackgroundDrawable(defaultDrawable);
            }
        }
    }

    @Override
    public void onFocusChange(View view, boolean b) {
        if(b){
            select_state = true;
            invalidate();
        }else{
            select_state = false;
            invalidate();
        }
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        switch (event.getAction()) {
            case MotionEvent.ACTION_DOWN:
                select_state = true;        //设置当前需求为 选中 状态
                setSelectAllOnFocus(false);     //禁用 获得焦点
                invalidate();                   //重新绘图
                break;
            case MotionEvent.ACTION_CANCEL:
            case MotionEvent.ACTION_UP:
                select_state = false;       //设置当前需求为 未选中 状态
                invalidate();               //重新绘图
                setSelectAllOnFocus(true);  //启用 获得焦点
                break;
        }
        return super.onTouchEvent(event);
    }
}
