package pw.hais.model;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Hais1992 on 2015/9/10.
 */
public class PostionMap<T> {
    private List<Obj> list;

    public PostionMap() {
        list = new ArrayList<>();
    }

    public void add(String key, T value) {
        list.add(new Obj(key, value));
    }

    public void add(int postion, String key, T value) {
        list.add(postion, new Obj(key, value));
    }

    public <T> T getValue(int postion) {
        return (T) list.get(postion).obj;
    }

    public String getKey(int postion) {
        return list.get(postion).key;
    }

    public int size() {
        return list.size();
    }


    class Obj<T> {
        public String key;
        public T obj;

        public Obj(String key, T obj) {
            this.key = key;
            this.obj = obj;
        }
    }


}