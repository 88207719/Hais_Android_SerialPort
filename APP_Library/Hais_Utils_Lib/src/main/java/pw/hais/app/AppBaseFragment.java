package pw.hais.app;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.ActionBar;

import com.google.gson.Gson;

import pw.hais.ui.LoadProgressDialog;

/**
 * Created by Hello_海生 on 15-7-28.
 */
public abstract class AppBaseFragment extends Fragment implements LoadProgressDialog.LoadDialog {
    protected String tag = "";              //当前TAG
    protected Activity context;              //方便设置监听器等。。
    protected Gson gson = UtilConfig.GSON;
    protected AppApplication baseApp = AppApplication.app;

//    public static Fragment newInstance(String json_value) {
//        Fragment fragment = new MyFragment();
//        Bundle bundle = new Bundle();
//        bundle.putString(UtilConfig.APP_ID+"gson",json_value+"");
//        fragment.setArguments(bundle);
//        return fragment;
//    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

//        String json_value = getArguments().getString(UtilConfig.APP_ID+"gson");

        tag = getClass().getSimpleName();   //获取 当前类名，方便 打log
        context = getActivity();

    }

//    @Nullable
//    @Override
//    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
//        return inflater.inflate(onCreateView(), container, false);
//    }
//
//    @Override
//    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
//        super.onViewCreated(view, savedInstanceState);
//
//        onInitView(view);
//        onLoadDateToView(view);
//    }

//    /**
//     * 设置布局视图
//     */
//    public abstract int onCreateView();
//
//    /**
//     * 初始化View
//     */
//    public abstract void onInitView(View view);
//
//    /**
//     * 加载数据到视图
//     */
//    public abstract void onLoadDateToView(View view);


    public void finish() {
        getActivity().finish();
    }

    public void popBackStack() {
        getFragmentManager().popBackStack();
    }

    public void onBackPressed() {
        int backStackCount = getFragmentManager().getBackStackEntryCount();
        if (backStackCount > 0) {
            getFragmentManager().popBackStack();
        } else {
            getActivity().finish();
        }
    }

    public AppBaseActivity getBaseActivity(){
        return (AppBaseActivity)getActivity();
    }

    public ActionBar getSupportActionBar(){
        return getBaseActivity().getSupportActionBar();
    }

    public FragmentManager getSupportFragmentManager(){
        return getBaseActivity().getSupportFragmentManager();
    }

    public void replaceToBackStack(Fragment newFragment,String backStackName){
        getSupportFragmentManager()
                .beginTransaction()
                .replace(getBaseActivity().V_ID, newFragment)
                .addToBackStack(backStackName)
                .commit();
    }

    public void replaceToBackStack(Fragment newFragment,String backStackName,String fragmentTag){
        getSupportFragmentManager()
                .beginTransaction()
                .replace(getBaseActivity().V_ID, newFragment,fragmentTag)
                .addToBackStack(backStackName)
                .commit();
    }

    public void replace(Fragment newFragment){
        getSupportFragmentManager()
                .beginTransaction()
                .replace(getBaseActivity().V_ID, newFragment)
                .commit();
    }



    /*----------------------------加载弹出窗------------------------------------*/
    private LoadProgressDialog loadDialog;    //菊花

    @Override
    public void loadDialogShow(String text) {
        if (loadDialog == null) loadDialog = new LoadProgressDialog(context);
        loadDialog.show(text);
    }

    @Override
    public void loadDialogDismiss() {
        if (loadDialog != null) loadDialog.dismiss();
    }
}
