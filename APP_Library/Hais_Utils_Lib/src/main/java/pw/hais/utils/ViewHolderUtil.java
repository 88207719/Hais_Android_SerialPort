package pw.hais.utils;

import android.app.Activity;
import android.content.Context;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;

import java.lang.reflect.Field;

import pw.hais.app.UtilConfig;

/**
 * View 工具类
 * Created by Hello_海生 on 2015/7/31.
 */
public class ViewHolderUtil {
    private static Context context = UtilConfig.CONTEXT;

    /**
     * 自动findViewById
     * @param activity  当前 activity
     */
    public static void initViews(Activity activity) {
        try {
            Field[] mFields = activity.getClass().getDeclaredFields();
            for (Field mField : mFields) {
                if(View.class.isAssignableFrom(mField.getType())){  //判断父类是否 view
                    int resourceId = context.getResources().getIdentifier(mField.getName(), "id", context.getApplicationContext().getPackageName());
                    mField.setAccessible(true);
                    mField.set(activity, activity.findViewById(resourceId));
                }
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    /**
     * 自动findViewById
     * @param fragment  当前 fragment
     */
    public static void initViews(Fragment fragment) {
        try {
            Field[] mFields = fragment.getClass().getDeclaredFields();
            for (Field mField : mFields) {
                if(View.class.isAssignableFrom(mField.getType())){  //判断父类是否 view
                    int resourceId = context.getResources().getIdentifier(mField.getName(), "id", context.getApplicationContext().getPackageName());
                    mField.setAccessible(true);
                    mField.set(fragment, fragment.getActivity().findViewById(resourceId));
                }
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }


    /**
     * 从当前View 中 自动 findViewById 到 实体类。
     * @param itemView  父View
     * @param clazz   实体类
     * @param <T>
     * @return
     */
    public static <T>T loadingViewToObject(View itemView,Class<T> clazz)
    {
        Context mContext = itemView.getContext();
        Object obj = null;
        try
        {
            obj = clazz.newInstance();
            Field[] mFields = obj.getClass().getDeclaredFields();
            for (Field mField : mFields)
            {
                int resourceId = mContext.getResources().getIdentifier(mField.getName(),"id",mContext.getApplicationContext().getPackageName());
                mField.setAccessible(true);
                mField.set(obj,itemView.findViewById(resourceId));
            }
        }
        catch (Exception ex)
        {
            ex.printStackTrace();
        }
        return (T)obj;
    }


    /**
     * 利用反射机制实例化对象,主要运用在Adapter复用
     * @param mContext 上下文对象
     * @param convertView   复用的view <b>(Adapter getView 方法中的convertView)</b>
     * @param layoutId 实例化布局ID
     * @param cls ViewHolder <b>(你的实体类)</b>
     * @return 装载后的view <b>(直接返回给getView)</b>
     */
    public static View loadingConvertView(Context mContext,View convertView,int layoutId,Class<?> cls)
    {
        try
        {
            if (null == convertView)
            {
                convertView = LayoutInflater.from(mContext).inflate(layoutId,null);
                Object obj = cls.newInstance();
                Field[] mFields = obj.getClass().getDeclaredFields();

                for (Field mField : mFields)
                {
                    //得到资源ID
                    int resourceId = mContext.getResources().getIdentifier(mField.getName(),"id",mContext.getApplicationContext().getPackageName());
                    //允许访问私有属性
                    mField.setAccessible(true);
                    //保存实例化后的资源
                    mField.set(obj,convertView.findViewById(resourceId));
                }
                convertView.setTag(obj);
            }
        }
        catch(Exception ex)
        {
            ex.printStackTrace();
        }
        return convertView;
    }


}
