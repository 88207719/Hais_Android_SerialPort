﻿#Hais_Android_Utils

#使用步骤：
	1、新建属于自己项目的 Application 然后继承AppApplication。
	2、新建属于自己项目的 BaseActivity 继承 AppBaseActivity。
	3、到 app包 中的 UtilConfig 配置所有工具类的信息。
	4、到此就能使用 Hais_Android_Utils 的所有功能。

#库中包的介绍
	该工具类有app、helper、http、model、sqlite、ui、utils、view。
	app    有一些用于继承的基类，如 AppApplication、AppBaseActivity、AppBaseFragment、AppAdapter、AppBasePopupWindow...
	helper 是Hais_Android_Utils的一些助手类，一般不使用
	http   是 网络请求的一些处理，包括 图片加载、文件下载、网络请求【支持添加header的post】。
	model  一些为了方便使用而封装的实体类。
	sqlite 简单的使用sqlite。
	ui     一些视图类，目前就只有一个 '菊花'的弹出窗。
	util   包含许多 工具类，可以适用于不同的场景。
	view   是一些简单的 控件 扩展。

	项目依赖 Gson、okhttp 。

**[有建议或疑问请到 [http://hais.pw](http://hais.pw/hais_android_utils.html) 留言]**	


============================================================
##一、pw.hais.app包：
	1、AppApplication
		(1)含有Activity 的统一管理， 和初始化工具。 退出App调用 exitApp() 方法。
		(2)有异常日记捕抓，捕抓类也可以上传日期到服务器。
		(3) onErrorListener方法 为 回调崩溃日记。
	
	2、AppBaseActivity
		特点：在activity中，如果xml中定义的ID，跟Activity中变量的名字一样，可免去findViewById
			如：
				XML中有一 TextView 的id 为  text_hello
				Activity中有一 TextView 类型的 text_hello 的属性(如：private TextView text_hello)
				那么就可以直接 使用 text_hello，而不需要  findViewById。
				此项功能可以到 UtilConfig 类中进行配置开关。

		(1)属性
			tag 属性：获取每个activity的名字作为 tag 方便打Log。
			context 属性： 当前activity 的上下文，方便 设置监听器等。
			gson 属性： 整个APP中的gson。

		(2)方法
			loadDialogShow(String text) 方法：显示 带文字的进度框，参数为null时不显示文字
			loadDialogDismiss() 方法：关闭 加载中的 Dialog框

	3、AppBaseFragment
    		(1)属性
    			tag 属性：获取每个activity的名字作为 tag 方便打Log。
    			context 属性： 当前activity 的上下文，方便 设置监听器等。
    			gson 属性： 整个APP中的gson。

    		(2)方法
    			onCreateView();	直接 return R.layout.main	加载布局
    			onInitView(View view);	在这个方法中 findViewbyId。【可和activity中免findView】
    			onLoadDateToView(View view);	这个方法 在 onInitView 之后显示。

    			finish();	关闭当前activity
    			popBackStack();	后退
    			onBackPressed();	后退到头后关闭当前activity

    			loadDialogShow(String text) 方法：显示 带文字的进度框，参数为null时不显示文字
    			loadDialogDismiss() 方法：关闭 加载中的 Dialog框


	4、BasePopupWindow
		(1)创建 PopupWindow 的时候 继承 BasePopupWindow。
		(2)在 PopupWindow 构造方法中 setLayout(Activity activity,int layout);
		(3) drawEnd 方法 是 当 PopupWindow绘制完成的回掉，可以在里面 findViewById， 和设置监听器等各种操作。
		(4)BasePopupWindow。 的showCenter(); 方法，activity 的中间
		(5)注：由于PopupWindow的限制，不能 在activity未绘制完成的时候调用，一般在 oncreate 中 实例化，  点击按钮等世界后 show();

	5、AppAdapter
		一个可以省去很多代码的Adapter，使用详情看Demo。

	6、UtilConfig
		配置整个工具类的参数。

##二、pw.hais.http 包
	1、网络Http请求
		(1) 普通请求 (监听器中， 可以是 JSONObject、String、等类型。)
			Http.get(String url, Map<String, String> params, OnHttpListener<?> listener);
			Http.post(String url, Map<String, String> params, OnHttpListener<?> listener);
			Http.post(String url, String body, OnHttpListener<?> listener);
			......
		(2)添加Header的请求
			Http.get(String url, Map<String, String> params, Map<String, String> header, OnHttpListener<?> listener);
			Http.post(String url, Map<String, String> params, Map<String, String> header, OnHttpListener<?> listener);
			......
		(3)其它特殊请求
			Http.post(String url, String body, OnHttpListener<?> listener);
			......

	2、HTTP文件上传
		Http.updateFile(String url, Map<String, String> params, File[] files, String[] fileKeys, OnHttpListener<?> listener);
		Http.updateFile(String url, Map<String, String> params, File file, String fileKey, OnHttpListener<?> listener);

	3、图片下载显示
		(1)加载网络图片，并显示  Http.displayImage(ImageView imageView, String url);
		(2)加载网络图片显示监听  Http.displayImage(ImageView imageView, String url, OnHttpListener<Bitmap> listener);
		(3)只缓存一张图片 		 Http.cacheImage(String url);
		(4)只缓存图片、带监听	 Http.cacheImage(String url, OnHttpListener<Bitmap> listener);
		(5)读取已经缓存好的图片	 Http.getCacheBitmap(String url);
		......

	4、文件下载(监听器可加进度条)
		Http.download(String url, String fileDir, OnHttpListener<String> listener);

	5、取消一个网络请求
		Http.cancel(String url);


##三、pw.hais.model包：
	PostionMap类：
		是一个，可以根据postion获取内容的 Map，一般配合List<PostionMap>使用,如：

		PostionMap<Fragment> fragmentList;
		fragmentList = new PostionMap<>();
        fragmentList.add("商品详情", OneFragment.newInstance(1));
        fragmentList.add("商品规格", TwoFragment.newInstance(2));
        fragmentList.add("商家详情", ThreeFramgent.newInstance(3));

        fragmentList.getKey(position);	//根据postion获取 标题
        fragmentList.getValue(position); //根据postion获取 object

##四、pw.hais.utils.sqlite包
	Sqlite类是 对于orm的简单封装。以下是使用步骤
	1、在 Model 类 的ID中 加上注解，auto设置自增，默认为自增
		@Id(auto=true) private int id;
	2、不需要加入  数据库操作的这么 注解
		@NoDB private int id;
	3、在需要使用的地方这么使用
		DBUtil.save(u);
		DBUtil.findById(User.class,"1");
	4、只支持简单数据类型。。

##五、pw.hais.ui包：
	1、LoadProgressDialog
		LoadProgressDialog loadDialog = new loadDialog(Context);
		loadDialog.show(); 	//显示菊花
		loadDialog.show("数据加载中.."); 	//显示带文字的菊花
		loadDialog.dismiss();	//关闭菊花
		(目前在Activity中，loadDialogShow 可直接调用此控件显示)

##六、pw.hais.utils包
	1、ApkInfoUtil 用于获取 当前APK 的 包名、版本号、版本名称、程序名称。
	2、AppInfoUtil 用于获取系统的 IMEI码、mac地址、CPU序列号、自定义用户唯一标识
	3、AppNetInfoUtil 用于判断  网络是否连接、否是wifi连接、否是移动网络、wifi是否打开、Gps是否打开、打开网络设置界面
	4、AppSystemUtil 用于 打开并安装APK文件、卸载程序、判断服务是否运行、停止服务
	5、CrashUtil 异常捕抓信息。在AppApplication已经默认启用。
	6、DownTime 由于自带的 CountDownTimer 的 calan函数失效，所以产生该工具类，用法一样。
	7、EmptyUtil 检查是否为空，字符串是否为空、列表是否为空、数组是否为空、对象是否为空、Map 是否为空
	8、GenericsUtils 反射活取父类属性。
	9、ImageUtil 可以Drawable转化为Bitmap、Bitmap转化为Drawable、Bitmap转换到Byte[]、保存图片到SD卡、从SD卡加载图片
	10、KeyBoardUtil 打开软键盘、关闭软键盘
	11、L  用于 打印Log，和 Toast 。 【在发布APP的时候可以到 UtilConfig 类配置 关闭所有Log 的输出】
	12、Md5Utils  MD5的加密。。。
	13、ReaderAssetsUtil 从Assets文件夹中获取文件并读取数据
	14、SDCardUtils 判断SDCard是否可用、 获取SD卡路径、获取SD卡的剩余容量、获取系统存储路径
	15、SPUtils 简化SharedPreferences 的封装，可以存储、读取任何Object对象，或 List
	16、ScreenUtil 获得屏幕高度、 获得屏幕宽度、获得状态栏的高度、获取屏幕截图、获取屏幕尺寸与密度.
	17、TimeUtil 将时间戳转为日期、转换中文对应的时段、剩余秒数转换
	18、UtilConfig 整个工具类的配置。
	19、ViewHolderUtils 加载视图，并反射控件，一般在 adapter 中使用。

	
##七、pw.hais.view包
	1、ButtonFocus
		扩展于Button使用方法跟Button 类似，只是增加了 按钮的点击效果。
		使用场景在于，当设置 一张图片为 按钮的背景时，失去了点击的效果的时候。
		
	2、ImagesFocus
		扩展于ImageView使用方法跟ImageView 类似，只是增加了点击效果。
		使用场景在于，使用imageView当作 button 来使用的时候，失去了点击的效果的时候。
	
	3、StrokeTextView
		扩展于TextView使用方法跟TextView 类似，只是给 TextView 增加了描边。
		
	4、TextIntChangeView
		扩展于TextView使用方法跟TextView 类似，当 setChange(100); 的时候，数值会动态改变。

	5、TextHtmlImageView
		扩展于TextView使用方法跟TextView 类似，当 setHtml("html字符串"); 的时候，会自动加载 html中的图片。


	
		