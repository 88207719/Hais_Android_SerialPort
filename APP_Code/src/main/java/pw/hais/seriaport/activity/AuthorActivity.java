package pw.hais.seriaport.activity;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import pw.hais.seriaport.R;


/**
 * Created by Hais1992 on 2015/12/11.
 */
public class AuthorActivity extends AppCompatActivity implements View.OnClickListener {
    private TextView text_msg;
    private Button btn_1, btn_2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_author);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        text_msg = (TextView) findViewById(R.id.text_msg);

        btn_1 = (Button) findViewById(R.id.btn_1);
        btn_2 = (Button) findViewById(R.id.btn_2);

        btn_1.setOnClickListener(this);
        btn_2.setOnClickListener(this);

        text_msg.setText("作者信息：" +
                "\n\t作者：Hello_海生" +
                "\n\t网址：http://hais.pw" +
                "\n\t邮箱：hais1992@163.com" +
                "\n\n" +
                "软件信息：" +
                "\n\t名称：" + ApkInfoUtil.getAppName(this) +
                "\n\t版本：" + ApkInfoUtil.getVersionName(this) +
                "\n\n" +
                "作品信息：" +
                "\n\t数据来源于网络，如有侵犯请联系作者。" +
                "\n\t本系列其它作品请点击‘寻找更多’获取。" +
                "\n\n" +
                "使用说明：" +
                "\n\t发送16进制Byte[]请按以下格式输入。" +
                "\n\t02 00 0A 01 01 30 30 30\n");
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_1:
                Uri uri = Uri.parse("http://hais.pw");
                startActivity(new Intent(Intent.ACTION_VIEW, uri));
                break;
            case R.id.btn_2:
                Uri uri_app = Uri.parse("http://app.hais.pw");
                startActivity(new Intent(Intent.ACTION_VIEW, uri_app));
                break;
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                break;
        }
        return super.onOptionsItemSelected(item);
    }
}
