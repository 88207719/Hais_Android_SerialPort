package pw.hais.seriaport.adapter;

import android.widget.TextView;

import pw.hais.app.AppAdapter;
import pw.hais.seriaport.R;

/**
 * Created by hais1992 on 2016/11/16/016.
 */

public class SpinnerAdapter extends AppAdapter<String, SpinnerAdapter.ViewHolder> {


    public SpinnerAdapter(String[] mList) {
        super(mList, R.layout.activity_main_spinner_item, ViewHolder.class);
    }

    @Override
    public void onBindView(int position, ViewHolder mViewHolder, String mItem) {
        mViewHolder.text_title.setText(mItem);
    }

    public static class ViewHolder extends AppAdapter.ViewHolder {
        public TextView text_title;
    }
}
